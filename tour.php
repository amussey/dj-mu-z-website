<html>
<head>
    <title>Tour Dates :: DJ Mu-Z</title>
    <script src="assets/mediaelement/build/jquery.js"></script>
    <script src="assets/mediaelement/build/mediaelement-and-player.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/style.css">
    <link rel="stylesheet" href="assets/mediaelement/mediaelementjs-skin/skin/mediaelementplayer.css" />
    <link rel="stylesheet" href="assets/mediaelement/mediaelementjs-skin/playerstyle.css" />
</head>
<body>
    <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
    <div id="wrapper">
        <?php require_once("assets/nav.php"); nav(); ?>
        <div id="content" style="width:800px; margin-top:30px;">
            For the most up to date information on Mu-Z shows and performances, follow the <a href="https://djmu-z.com/facebook">Facebook page</a>.

            <div class="radioshow" style="margin-bottom:30px;">
                <div style="width:100%; float:left; border-top: 1px solid #000; border-bottom: 1px solid #000;">
                    <div style="float:left; width: 300px; font-weight:bold; margin-top:25px;">Comedy night @ Top of the Stairs</div>
                    <div style="float:left; font-size:12px; margin-top:30px; width:130px;">Blacksburg, VA</div>
                    <div style="float:left; font-size:12px; margin-top:30px; width:130px;">Aug 26, 2015</div>
                    <div style="float:right; text-align:center; margin-top:6px;"><h4><a href="https://www.facebook.com/events/1486613724969184/?ref=2&amp;ref_dashboard_filter=past&amp;sid_reminder=6562725163752751104&amp;action_history=null" target="_blank">$6, Tickets at the Door</a></h4></div>
                </div>
            </div>
            <br style="clear:both" />
        </div>
        <div id="copyright" style="margin-top:30px;">&copy;<?=date("Y") ?> Andrew Mussey.  All rights reserved.</div>
    </div>

</body>
</html>
