# Mu-Z Website

This is the source for the homepage of [Mu-Z](http://djmu-z.com).

A particular section of interest in this code is the mouse-activated M graphic from the front page of the site (located in `index.php`).  This graphic can currently be seen in action at [DJMu-Z.com](http://djmu-z.com).

![](https://bytebucket.org/amussey/dj-mu-z-website/raw/109412a63d0e278112a16c139da949704c3a03a7/images/screenshots/Summer2014.jpg)




For the previous version of the site, checkout the branch `original-site`.

![](https://bytebucket.org/amussey/dj-mu-z-website/raw/109412a63d0e278112a16c139da949704c3a03a7/images/screenshots/Summer2013.jpg)