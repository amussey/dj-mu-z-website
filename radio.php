<html>
<head>
    <title>Radio :: DJ Mu-Z</title>
    <script src="assets/mediaelement/build/jquery.js"></script>
    <script src="assets/mediaelement/build/mediaelement-and-player.min.js"></script>
    <link rel="stylesheet" type="text/css" href="assets/style.css">
    <link rel="stylesheet" href="assets/mediaelement/mediaelementjs-skin/skin/mediaelementplayer.css" />
    <link rel="stylesheet" href="assets/mediaelement/mediaelementjs-skin/playerstyle.css" />
</head>
<body>
    <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
    <div id="wrapper">
        <?php require_once("assets/nav.php"); nav(); ?>
        <div id="content" style="width:800px; margin-top:30px;">
            Mu-Z hosts a weekly radio show on <a href="https://www.facebook.com/KRackRadio">KRACK Radio</a> called the Electro Lunch.<br />
            The playlists for the previous shows will be available online here.
            <script>
            function changeTime(player, time_select) {
                player.pause();
                new_time = parseInt(time_select.split(":")[0])*60+parseInt(time_select.split(":")[1]);
                player.play();
                setTimeout(function() { player.setCurrentTime(new_time); }, 500);
            }
            </script>

            <div class="radioshow">
                <h3>Electro Lunch from September 18, 2013</h3>
                <audio style="margin-left: 50px;" width="600" id="radioshow3"
                    src="https://1109768fe10ebe22ea80-c452c77c388f7dd9cf1a224423883f38.ssl.cf5.rackcdn.com/Electro Lunch 3.mp3"
                    type="audio/mp3" controls="controls">
                    <object width="600" height="60" type="application/x-shockwave-flash" data="../assets/mediaelement/build/flashmediaelement.swf">
                        <param name="movie" value="flashmediaelement.swf" />
                        <param name="flashvars" value="controls=true&amp;poster=myvideo.jpg&amp;file=https://1109768fe10ebe22ea80-c452c77c388f7dd9cf1a224423883f38.ssl.cf5.rackcdn.com/Electro Lunch 3.mp3" />
                    </object>
                </audio>

                <script>
                var radioshow3 = new MediaElementPlayer('#radioshow3', {
                    flashName: 'flashmediaelement.swf',
                    success: function (mediaElement, domObject) { }
                });

                </script>
                <div class="rowshow_info">
                    <table>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">00:00</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">05:30</a></td>
                            <td class="trackname">Breathe Caroline - Blackout (Wideboys Remix)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">01:00</a></td>
                            <td></td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())"></a></td>
                            <td class="trackname">ID</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">05:20</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">09:04</a></td>
                            <td class="trackname">A$AP Rocky - Wild For The Night (Dog Blood Remix) (Mu-Z Radio Cut)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">08:57</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">12:39</a></td>
                            <td class="trackname">Linkin Park feat. Steve Aoki - Light That Never Comes</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">12:41</a></td>
                            <td></td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())"></a></td>
                            <td class="trackname">ID</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">12:41</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">16:35</a></td>
                            <td class="trackname">deadmau5 feat. Gerard Way - Professional Greifers (Vocal Mix)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">16:08</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">23:07</a></td>
                            <td class="trackname">Hollywood Undead - Levitate (Digital Dog Remix)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">23:07</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">26:17</a></td>
                            <td class="trackname">3lau - I-3low</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">23:09</a></td>
                            <td></td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())"></a></td>
                            <td class="trackname">ID</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">26:10</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">31:15</a></td>
                            <td class="trackname">Korn (feat. Skrillex) - Shut Up (Mu-Z Radio Cut)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">31:09</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">34:52</a></td>
                            <td class="trackname">Sum 41 - Fat Lip (Acetronik Remix)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">34:55</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">38:00</a></td>
                            <td class="trackname">Fall Out Boy - Alone Together (Krewella Remix)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">38:00</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">42:56</a></td>
                            <td class="trackname">Innerpartysystem - Don't Stop (Wuki Rebirth)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">38:01</a></td>
                            <td></td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())"></a></td>
                            <td class="trackname">ID</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">42:56</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">46:22</a></td>
                            <td class="trackname">Pegboard Nerds feat. Splitbreed - We Are One</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">46:22</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">50:52</a></td>
                            <td class="trackname">Nick Tayer - Like Boom (Nick Thayer Remix)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">50:50</a></td>
                            <td></td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())"></a></td>
                            <td class="trackname">Cazzette - Beam Me Up</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())">51:46</a></td>
                            <td></td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow3, $(this).html())"></a></td>
                            <td class="trackname">ID</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="radioshow">
                <h3>Electro Lunch from September 4, 2013</h3>
                <audio style="margin-left: 50px;" width="600" id="radioshow1"
                    src="https://1109768fe10ebe22ea80-c452c77c388f7dd9cf1a224423883f38.ssl.cf5.rackcdn.com/Electro Lunch 1.mp3"
                    type="audio/mp3" controls="controls">
                    <object width="600" height="60" type="application/x-shockwave-flash" data="assets/mediaelement/build/flashmediaelement.swf">
                        <param name="movie" value="flashmediaelement.swf" />
                        <param name="flashvars" value="controls=true&amp;poster=myvideo.jpg&amp;file=https://1109768fe10ebe22ea80-c452c77c388f7dd9cf1a224423883f38.ssl.cf5.rackcdn.com/Electro Lunch 1.mp3" />        
                    </object>
                </audio>
            
                <script>
                var radioshow1 = new MediaElementPlayer('#radioshow1', {
                    flashName: 'flashmediaelement.swf',
                    success: function (mediaElement, domObject) { }
                });

                </script>
                <div class="rowshow_info">
                    <table>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">00:00</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">04:50</a></td>
                            <td class="trackname">Capital Cities - Safe &amp; Sound (Cash Cash Remix) </td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">04:37</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">08:42</a></td>
                            <td class="trackname">Krewella - Play Hard (Explicit)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">08:29</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">11:56</a></td>
                            <td class="trackname">3lau - B#tch City</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">11:45</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">15:40</a></td>
                            <td class="trackname">Zedd (feat. Lucky Date &amp; Ellie Goulding) - Fall Into The Sky</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">15:37</a></td>
                            <td></td>
                            <td></td>
                            <td class="trackname">ID</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">15:39</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">18:40</a></td>
                            <td class="trackname">Dada Life - Feed The Dada</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">18:40</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">19:55</a></td>
                            <td class="trackname">Savant - Sustainer</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">19:55</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">23:42</a></td>
                            <td class="trackname">3OH!3 - Make It Easy</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">23:28</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">26:49</a></td>
                            <td class="trackname">Karmin - Broken Hearted (R3hab Remix)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">26:49</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">32:43</a></td>
                            <td class="trackname">Maroon 5 - Payphone (Cosmic Dawn Remix)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">32:37</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">36:33</a></td>
                            <td class="trackname"><a href="https://soundcloud.com/dj-mu-z/more-lego-house-bootleg-re">Mu-Z - More (Lego House Bootleg)</a></td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">36:36</a></td>
                            <td></td>
                            <td></td>
                            <td class="trackname">ID</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">36:33</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">42:05</a></td>
                            <td class="trackname">Dune - Princess of Valentina (Alesso Remix)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">42:00</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">48:18</a></td>
                            <td class="trackname">Uplifted - I Like Turtles</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">48:10</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">53:30</a></td>
                            <td class="trackname">Calvin Harris (feat. Ellie Goulding) - Need Your Love (Nicky Romero Remix)</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">49:04</a></td>
                            <td></td>
                            <td></td>
                            <td class="trackname">ID</td>
                        </tr>
                        <tr>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">53:03</a></td>
                            <td>-</td>
                            <td><a href="#" onclick="javascript:changeTime(radioshow1, $(this).html())">57:07</a></td>
                            <td class="trackname">Boys Noize - &amp; Down</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div id="copyright">&copy;<?=date("Y") ?> Andrew Mussey.  All rights reserved.</div>
    </div>

</body>
</html>
