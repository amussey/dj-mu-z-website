<?php
function nav($path = ".") {
?>
        <div id="header">
            <a href="/"><div id="logo"></div></a>
            <div id="icons">
                <a href="https://soundcloud.com/dj-mu-z"><img border="0" src="<?=$path ?>/images/icon_soundcloud.png" class="icons" /></a>
                <a href="https://www.facebook.com/pages/DJ-Mu-Z/175278072532686"><img border="0" src="<?=$path ?>/images/icon_facebook.png" class="icons" /></a>
                <a href="https://www.twitter.com/amussey"><img border="0" src="<?=$path ?>/images/icon_twitter.png" class="icons" /></a>
            </div>
        </div>
        <div id="nav">
            <a href="/tour"><div class="nav1"><img border="0" class="navimg" src="<?=$path ?>/images/nav_dates.png"></div></a>
            <a href="/radio"><div class="nav2"><img border="0" class="navimg" src="<?=$path ?>/images/nav_radio.png"></div></a>
            <a href="https://soundcloud.com/dj-mu-z"><div class="nav2"><img border="0" class="navimg" src="<?=$path ?>/images/nav_downloads.png"></div></a>
            <a href="https://www.facebook.com/pages/DJ-Mu-Z/175278072532686"><div class="nav2"><img border="0" class="navimg" src="<?=$path ?>/images/nav_contact.png"></div></a>
            <a href="/press"><div class="nav2"><img border="0" class="navimg" src="<?=$path ?>/images/nav_press.png"></div></a>
        </div>
<?php
}
?>
