<html>
<head>
    <title>DJ Mu-Z</title>
    <link rel="stylesheet" type="text/css" href="assets/style.css">
</head>
<body>
    <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
    <div id="wrapper">
        <?php require_once("assets/nav.php"); nav() ?>
        <div id="content" class="giantm"></div>
        <div id="copyright">&copy;<?=date("Y") ?> Andrew Mussey.  All rights reserved.</div>
    </div>
    <script>
    jQuery(function($) {
        var currentMousePos = { x: -0};
        $(".giantm").mousemove(function(event) {
            currentMousePos.x = event.pageX - $(".giantm").offset().left;
            $(".giantm").css("background-position", "-" + (parseInt(currentMousePos.x/100)*1100) + "px 0px");
        });
    });
    </script>
</body>
</html>
