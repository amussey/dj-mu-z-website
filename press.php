<html>
<head>
    <title>Press :: DJ Mu-Z</title>
    <link rel="stylesheet" type="text/css" href="assets/style.css">
</head>
<body>
    <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
    <div id="wrapper">
        <?php require_once("assets/nav.php"); nav("."); ?>
        <div id="content" style="width:800px;">
            <p class="content">&nbsp;</p>
            <p class="content">Bassmob at <a href="https://www.facebook.com/pages/Os-Lounge-Blacksburg/300807893303412?ref=br_tf">O's Lounge in Blacksburg</a> (Spring 2013)</p>
            <p class="content"><center><img src="https://i.imgur.com/ChDixDv.jpg" width="712" /></center>
            </p>

            <p class="content">&nbsp;</p>
            <p class="content">Parties for <a href="https://reddit.com/r/VirginiaTech">Redditors</a> (Spring 2012)</p>
            <p class="content"><center><img src="https://i.imgur.com/lP43W.jpg" width="712" /></center>
            </p>


            <p class="content">&nbsp;</p>
            <p class="content"><a href="http://www.aoe.vt.edu/">AOE</a> Formal (Winter 2011)</p>
            <p class="content"><center><img src="https://djmu-z.com/images/312282_2017965420032_1570770168_31576477_1776448209_n.jpg" width="712" /></center>
            </p>

<!--             <p class="content">&nbsp;</p>
            <p class="content">House Party on Halloween 2011</p>
            <p class="content"><center>
            <object width="712" height="399" ><param name="allowfullscreen" value="true" /><param name="movie" value="http://www.facebook.com/v/2054154604739" /><embed src="http://www.facebook.com/v/2054154604739" type="application/x-shockwave-flash" allowfullscreen="true" width="712" height="399"></embed></object></center>
            </p>


            <p class="content">&nbsp;</p>
            <p class="content"><a href="http://topofthestairs.com/">Top of the Stairs</a> in Blacksburg, VA (Summer 2011)</p>
            <p class="content"><center>
            <object width="712" height="399" ><param name="allowfullscreen" value="true" /><param name="movie" value="http://www.facebook.com/v/1762275947955" /><embed src="http://www.facebook.com/v/1762275947955" type="application/x-shockwave-flash" allowfullscreen="true" width="712" height="399"></embed></object></center>
            </p > -->



            <p class="content">&nbsp;</p>
            <p class="content">Chi Omega's performance in Delta Gamma's Anchorsplash 2010</p>
            <p class="content">
                <center>
                <iframe width="712" height="534" src="https://www.youtube.com/embed/wiZKvXerjVA" frameborder="0" allowfullscreen></iframe>
                </center>
            </p>

        </div>
        <div id="copyright">&copy;<?=date("Y") ?> Andrew Mussey.  All rights reserved.</div>
    </div>

</body>
</html>
